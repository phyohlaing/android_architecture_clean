package com.pattern.clean.domain

import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNull
import org.junit.Test
import com.pattern.clean.domain.model.Photo as DomainPhoto
import com.pattern.clean.presentation.model.Photo as PresentationPhoto

/**
 * Created by Phyo (Billy) Hlaing on 26/11/18.
 */
class PhotoMapperTest {

    @Test
    fun on_mapping_photo_response_will_map_to_domain_photo_test() {
        val mockDomainPhoto = DomainPhoto(albumId = 1,
                                              url = "google.com")

        val photo = mapToPresentationPhoto(mockDomainPhoto)

        assertEquals(1, photo!!.albumId)
        assertEquals("google.com", photo!!.url)
    }

    @Test
    fun on_mapping_null_photo_response_will_map_to_null_test() {
        assertNull(mapToPresentationPhoto(null))
    }
}