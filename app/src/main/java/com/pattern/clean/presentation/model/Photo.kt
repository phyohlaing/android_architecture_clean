package com.pattern.clean.presentation.model

data class Photo(val albumId: Int,
                 val url: String)

