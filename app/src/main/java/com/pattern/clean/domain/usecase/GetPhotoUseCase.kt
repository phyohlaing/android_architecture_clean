package com.pattern.clean.domain.usecase

import com.pattern.clean.data.PhotoRepository
import com.pattern.clean.data.PhotoService
import com.pattern.clean.domain.mapToPresentationPhoto
import com.pattern.clean.domain.model.Photo
import com.pattern.clean.presentation.model.Photo as PresentationPhoto

/**
 * Created by Phyo (Billy) Hlaing on 7/11/18.
 */
class GetPhotoUseCase constructor(private val photoRepository: PhotoRepository,
                                  var getPhotoCallBack: GetPhotoCallback? = null) : PhotoService.PhotoServiceCallback {

    fun getPhotoFromRemote() = photoRepository.getPhotoFromRemote(this)

    //region getPhotoFromRemote callbacks
    override fun onFailurePhotoServiceResponse(message: String) {
        getPhotoCallBack?.onGetPhotoFailure(message)
    }

    override fun onSuccessPhotoServiceResponse(photo: Photo?) {
        getPhotoCallBack?.onGetPhotoSuccess(mapToPresentationPhoto(photo))
    }
    //endregion

    interface GetPhotoCallback {
        fun onGetPhotoSuccess(photo: PresentationPhoto?)
        fun onGetPhotoFailure(message: String)
    }
}