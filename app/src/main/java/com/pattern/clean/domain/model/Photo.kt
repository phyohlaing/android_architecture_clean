package com.pattern.clean.domain.model

/**
 * Created by Phyo (Billy) Hlaing on 7/11/18.
 */
data class Photo(val albumId: Int,
                 val url: String)
