package com.pattern.clean.domain

import android.graphics.Bitmap
import com.pattern.clean.data.DownloadPhotoRepository
import com.pattern.clean.domain.usecase.DownloadPhotoForAlbumOneUseCase
import com.pattern.clean.domain.usecase.DownloadPhotoForAlbumOneUseCase.DownloadPhotoForAlbumOneCallBack
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.Test

/**
 * Created by Phyo (Billy) Hlaing on 26/11/18.
 */
class DownloadPhotoForAlbumOneUseCaseTest {

    @RelaxedMockK private lateinit var downloadPhotoRepository: DownloadPhotoRepository
    @RelaxedMockK private lateinit var downloadPhotoForAlbumOneCallBack: DownloadPhotoForAlbumOneCallBack
    @RelaxedMockK private lateinit var mockBitmap: Bitmap

    private val downloadPhotoForAlbumOneUseCase: DownloadPhotoForAlbumOneUseCase

    init {
        MockKAnnotations.init(this)
        downloadPhotoForAlbumOneUseCase = DownloadPhotoForAlbumOneUseCase(downloadPhotoRepository, downloadPhotoForAlbumOneCallBack)
    }

    @Test
    fun download_photo_for_album_1_test() {
        downloadPhotoForAlbumOneUseCase.download(1, "google.com")

        verify { downloadPhotoRepository.downloadFromUrl("google.com") }
    }

    @Test
    fun does_not_download_photo_for_non_album1_test() {
        downloadPhotoForAlbumOneUseCase.download(2, "google.com")

        verify(exactly = 0) { downloadPhotoRepository.downloadFromUrl("google.com") }
    }

    @Test
    fun on_successful_download_will_deliver_bitmap_photo_test() {
        downloadPhotoForAlbumOneUseCase.onSuccess(mockBitmap)

        verify { downloadPhotoForAlbumOneCallBack.onDownloadSuccess(mockBitmap) }
    }

    @Test
    fun on_download_failure_will_deliver_error_message_test() {
        downloadPhotoForAlbumOneUseCase.onFailure("Error message")

        verify { downloadPhotoForAlbumOneCallBack.onDownloadFailure("Error message") }
    }

    @Test
    fun on_download_failure_without_error_message_will_be_ignored_test() {
        downloadPhotoForAlbumOneUseCase.onFailure(null)

        verify(exactly = 0) { downloadPhotoForAlbumOneCallBack.onDownloadFailure(null) }
    }
}