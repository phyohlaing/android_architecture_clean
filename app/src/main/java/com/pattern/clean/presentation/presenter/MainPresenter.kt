package com.pattern.clean.presentation.presenter

import android.graphics.Bitmap
import com.pattern.clean.presentation.model.Photo

import com.pattern.clean.domain.usecase.DownloadPhotoForAlbumOneUseCase
import com.pattern.clean.domain.usecase.GetPhotoUseCase

class MainPresenter(private val getPhotoUseCase: GetPhotoUseCase,
                    private val downloadPhotoForAlbumOneUseCase: DownloadPhotoForAlbumOneUseCase) : GetPhotoUseCase.GetPhotoCallback, DownloadPhotoForAlbumOneUseCase.DownloadPhotoForAlbumOneCallBack {

    private lateinit var display: Display

    fun inject(display: Display) {
        this.display = display
    }

    //region Life-cycle
    fun onStart() {
        getPhotoUseCase.getPhotoFromRemote()

        //callbacks
        getPhotoUseCase.getPhotoCallBack = this
        downloadPhotoForAlbumOneUseCase.downloadPhotoForAlbumOneCallBack = this
    }

    fun onStop() {
        getPhotoUseCase.getPhotoCallBack = null
        downloadPhotoForAlbumOneUseCase.downloadPhotoForAlbumOneCallBack = null
    }
    //end region

    //region GetPhoto callbacks
    override fun onGetPhotoSuccess(photo: Photo?) {
        photo?.let {
            downloadPhotoForAlbumOneUseCase.download(it.albumId, it.url)
        }
    }

    override fun onGetPhotoFailure(message: String) {
        display.showErrorMessage(message)
    }
    //endregion

    //region Download photo callbacks
    override fun onDownloadSuccess(bitmap: Bitmap) {
        display.showPhoto(bitmap)
    }

    override fun onDownloadFailure(message: String?) {
        message?.let {
            display.showErrorMessage(message)
        }
    }
    //endregion

    interface Display {
        fun showPhoto(bitmap: Bitmap)
        fun showErrorMessage(errorMessage: String)
    }
}