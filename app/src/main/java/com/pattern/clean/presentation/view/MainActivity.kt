package com.pattern.clean.presentation.view

import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.pattern.clean.R
import kotlinx.android.synthetic.main.activity_main.*
import com.pattern.clean.MVPApplication
import com.pattern.clean.presentation.presenter.MainPresenter

class MainActivity : AppCompatActivity(), MainPresenter.Display {

    private lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initPresenter()
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun showErrorMessage(errorMesage: String) {
        Toast.makeText(this, errorMesage, Toast.LENGTH_LONG).show()
    }

    override fun showPhoto(bitmap: Bitmap) {
        photoView.setImageBitmap(bitmap)
    }

    private fun initPresenter(){
        val mvpApp = application as MVPApplication

        presenter = MainPresenter(
                mvpApp.getPhotoUseCase(),
                mvpApp.getDownloadPhotoOneUseCase())

        presenter.inject(this)
    }

}
