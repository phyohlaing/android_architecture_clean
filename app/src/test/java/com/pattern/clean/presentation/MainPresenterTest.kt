package com.pattern.clean.presentation

import android.graphics.Bitmap
import com.pattern.clean.presentation.model.Photo
import com.pattern.clean.data.PhotoRepository
import com.pattern.clean.domain.usecase.DownloadPhotoForAlbumOneUseCase
import com.pattern.clean.domain.usecase.GetPhotoUseCase
import com.pattern.clean.presentation.presenter.MainPresenter
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.Test
import org.junit.Before

class MainPresenterTest {

    @RelaxedMockK private lateinit var photoRepository: PhotoRepository
    @RelaxedMockK private lateinit var getPhotoCallBack: GetPhotoUseCase.GetPhotoCallback


    @RelaxedMockK  private lateinit var downloadPhotoUseCase: DownloadPhotoForAlbumOneUseCase
    @RelaxedMockK private lateinit var mockDisplay: MainPresenter.Display

    @RelaxedMockK private lateinit var mockBitmap: Bitmap

    private var mainPresenter: MainPresenter
    private var mockPhotoUseCase: GetPhotoUseCase

    private val mockPhoto = Photo(1, "url.com")

    init {
        MockKAnnotations.init(this)
        mockPhotoUseCase = GetPhotoUseCase(photoRepository, getPhotoCallBack)
        mainPresenter = MainPresenter(mockPhotoUseCase, downloadPhotoUseCase)
    }

    @Before
    fun injectMockDisplay() {
        mainPresenter.inject(mockDisplay)
    }

    @Test
    fun onStart_get_photo_test() {
        mainPresenter.onStart()

        verify { mockPhotoUseCase.getPhotoFromRemote() }
    }

    @Test
    fun on_successful_get_photo_with_response_will_download_photo_test() {
        mainPresenter.onGetPhotoSuccess(mockPhoto)

        verify { downloadPhotoUseCase.download(1, "url.com") }
    }

    @Test
    fun on_successful_get_photo_with_no_response_will_be_ignored_test() {
        mainPresenter.onGetPhotoSuccess(null)

        verify(exactly = 0) { downloadPhotoUseCase.download(any(), any()) }
    }

    @Test
    fun on_fail_get_photo_will_display_error_message_test() {
        mainPresenter.onGetPhotoFailure("Error message")

        verify { mockDisplay.showErrorMessage("Error message") }
    }

    @Test
    fun on_download_photo_successfully_will_show_photo_test() {
        mainPresenter.onDownloadSuccess(mockBitmap)

        verify { mockDisplay.showPhoto(mockBitmap) }
    }

    @Test
    fun on_download_photo_fail_will_show_error_message_if_applicable_test() {
        mainPresenter.onDownloadFailure("Error message")

        verify { mockDisplay.showErrorMessage("Error message") }
    }

    @Test
    fun on_download_photo_fail_will_be_ignored_if_error_message_not_applicable_test() {
        mainPresenter.onDownloadFailure(null)

        verify(exactly = 0) { mockDisplay.showErrorMessage(any()) }
    }
}
