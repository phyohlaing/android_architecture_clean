package com.pattern.clean.data

import com.pattern.clean.data.model.PhotoResponse
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNull
import org.junit.Test

/**
 * Created by Phyo (Billy) Hlaing on 26/11/18.
 */
class PhotoMapperTest {

    @Test
    fun on_mapping_photo_response_will_map_to_domain_photo_test() {
        val mockPhotoResponse = PhotoResponse(albumId = 1,
                                              id = 99,
                                              title = "photo title",
                                              url = "google.com",
                                              thumbnailUrl = "thumbnail")

        val photo = mapToDomainPhoto(mockPhotoResponse)

        assertEquals(1, photo!!.albumId)
        assertEquals("google.com", photo!!.url)
    }

    @Test
    fun on_mapping_null_photo_response_will_map_to_null_test() {
        assertNull(mapToDomainPhoto(null))
    }
}