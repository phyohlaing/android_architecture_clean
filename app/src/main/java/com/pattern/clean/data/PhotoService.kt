package com.pattern.clean.data

import com.pattern.clean.data.model.PhotoResponse
import com.pattern.clean.domain.model.Photo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

class PhotoService : PhotoRepository {

    private val retrofit = Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()!!

    private val client: PhotoClient

    init {
        client = retrofit.create(PhotoClient::class.java)
    }

    override fun getPhotoFromRemote(callback: PhotoServiceCallback) {

        client.getPhoto().enqueue(object : Callback<PhotoResponse> {
            override fun onResponse(call: Call<PhotoResponse>?, response: Response<PhotoResponse>?) {
                if (response != null && response.isSuccessful) {
                    callback.onSuccessPhotoServiceResponse(mapToDomainPhoto(response.body()))
                } else {
                    callback.onFailurePhotoServiceResponse("Retrieve photo failed!")
                }

            }

            override fun onFailure(call: Call<PhotoResponse>?, t: Throwable?) {
                callback.onFailurePhotoServiceResponse("Retrieve photo failed!")
            }
        })
    }

    interface PhotoServiceCallback {
        fun onSuccessPhotoServiceResponse(photo: Photo?)
        fun onFailurePhotoServiceResponse(message: String)
    }

    interface PhotoClient {
        @GET("photos/1")
        fun getPhoto(): Call<PhotoResponse>
    }

}