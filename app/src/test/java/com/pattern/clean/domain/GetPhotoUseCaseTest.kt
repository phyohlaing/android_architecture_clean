package com.pattern.clean.domain

import com.pattern.clean.data.PhotoRepository
import com.pattern.clean.domain.usecase.GetPhotoUseCase
import com.pattern.clean.domain.usecase.GetPhotoUseCase.GetPhotoCallback
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import com.pattern.clean.presentation.model.Photo as PresentationPhoto
import com.pattern.clean.domain.model.Photo as DomainPhoto
import org.junit.Test

/**
 * Created by Phyo (Billy) Hlaing on 14/11/18.
 */

class GetPhotoUseCaseTest {

    @RelaxedMockK private lateinit var photoRepository: PhotoRepository
    @RelaxedMockK private lateinit var getPhotoCallBack: GetPhotoCallback

    private var mockPhotoUseCase: GetPhotoUseCase
    private val mockPhoto = DomainPhoto(1, "google.com")

    init {
        MockKAnnotations.init(this)
        mockPhotoUseCase = GetPhotoUseCase(photoRepository, getPhotoCallBack)
    }

    @Test
    fun fail_to_get_photo_will_deliver_error_message_test() {
        mockPhotoUseCase.onFailurePhotoServiceResponse("Fail message")

        verify { getPhotoCallBack.onGetPhotoFailure("Fail message") }
    }

    @Test
    fun successful_get_photo_will_deliver_photo_test() {
        mockPhotoUseCase.onSuccessPhotoServiceResponse(mockPhoto)

        verify { getPhotoCallBack.onGetPhotoSuccess(mapToPresentationPhoto(mockPhoto)) }
    }
}