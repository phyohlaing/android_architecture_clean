package com.pattern.clean.data

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import java.net.URL

class DownloadPhotoService : AsyncTask<String, Unit, Bitmap>(), DownloadPhotoRepository {

    private var exception: Exception? = null

    private lateinit var callback: DownloadPhotoCallBack

    override fun setDownloadPhotoCallBack(cb: DownloadPhotoCallBack): DownloadPhotoService {
        callback = cb
        return this
    }

    override fun downloadFromUrl(urlString: String) {
        execute(urlString)
    }

    override fun doInBackground(vararg args: String): Bitmap? {
        val url = URL(args[0])
        return try {
            BitmapFactory.decodeStream(url.openConnection().getInputStream())
        } catch (e: Exception) {
            this.exception = e
            null
        }
    }

    override fun onPostExecute(bitmap: Bitmap?) {
        bitmap?.let {
            callback.onSuccess(it)
        } ?: callback.onFailure(exception!!.message)
    }

    interface DownloadPhotoCallBack {
        fun onSuccess(bitmap: Bitmap)
        fun onFailure(message: String?)
    }
}