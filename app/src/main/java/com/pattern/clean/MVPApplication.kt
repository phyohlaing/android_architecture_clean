package com.pattern.clean

import android.app.Application
import com.pattern.clean.data.PhotoService
import com.pattern.clean.data.DownloadPhotoService
import com.pattern.clean.domain.usecase.DownloadPhotoForAlbumOneUseCase
import com.pattern.clean.domain.usecase.GetPhotoUseCase

class MVPApplication : Application() {
    private val photoService = PhotoService()
    private val downloadPhotoService = DownloadPhotoService()

    private val getPhotoUseCase = GetPhotoUseCase(photoService)
    private val downloadPhotoForAlbumOneUseCase = DownloadPhotoForAlbumOneUseCase(downloadPhotoService)

    fun getPhotoUseCase() = getPhotoUseCase

    fun getDownloadPhotoOneUseCase() = downloadPhotoForAlbumOneUseCase
}