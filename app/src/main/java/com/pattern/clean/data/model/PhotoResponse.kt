package com.pattern.clean.data.model

/**
 * Created by Phyo (Billy) Hlaing on 7/11/18.
 */
data class PhotoResponse(val albumId: Int,
                 val id: Int,
                 val title: String,
                 val url: String,
                 val thumbnailUrl: String)