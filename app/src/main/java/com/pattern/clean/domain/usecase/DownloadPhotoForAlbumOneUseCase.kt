package com.pattern.clean.domain.usecase

import android.graphics.Bitmap
import com.pattern.clean.data.DownloadPhotoService.DownloadPhotoCallBack
import com.pattern.clean.data.DownloadPhotoRepository

/**
 * Created by Phyo (Billy) Hlaing on 19/11/18.
 */

class DownloadPhotoForAlbumOneUseCase(private val downloadPhotoRepository: DownloadPhotoRepository,
                                      var downloadPhotoForAlbumOneCallBack: DownloadPhotoForAlbumOneCallBack? = null) : DownloadPhotoCallBack {

    init {
        downloadPhotoRepository.setDownloadPhotoCallBack(this)
    }

    fun download(albumId: Int, url: String) = downloadPhotoRepository.apply {
        when (albumId) {
            1 -> downloadFromUrl(url)
            else -> downloadPhotoForAlbumOneCallBack?.onDownloadFailure("Error: This use case downloads photos for Album 1 only")
        }

    }

    override fun onSuccess(bitmap: Bitmap) {
        downloadPhotoForAlbumOneCallBack?.onDownloadSuccess(bitmap)
    }

    override fun onFailure(message: String?) {
        message?.let {
            downloadPhotoForAlbumOneCallBack?.onDownloadFailure(message)
        }
    }

    interface DownloadPhotoForAlbumOneCallBack {
        fun onDownloadSuccess(bitmap: Bitmap)
        fun onDownloadFailure(message: String?)
    }
}