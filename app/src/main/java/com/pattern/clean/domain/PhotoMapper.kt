package com.pattern.clean.domain

import com.pattern.clean.domain.model.Photo as DomainPhoto
import com.pattern.clean.presentation.model.Photo as PresentationPhoto

/**
 * Created by Phyo (Billy) Hlaing on 26/11/18.
 */
fun mapToPresentationPhoto(photo: DomainPhoto?): PresentationPhoto? = photo?.let { PresentationPhoto(photo.albumId, photo.url) }