package com.pattern.clean.data

import com.pattern.clean.data.model.PhotoResponse
import com.pattern.clean.domain.model.Photo

/**
 * Created by Phyo (Billy) Hlaing on 8/11/18.
 */
fun mapToDomainPhoto(photoResponse: PhotoResponse?): Photo? =
        photoResponse?.let {
            Photo(it.albumId,it.url)
        }