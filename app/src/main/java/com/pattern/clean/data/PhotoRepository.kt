package com.pattern.clean.data

interface PhotoRepository{
    fun getPhotoFromRemote(callback: PhotoService.PhotoServiceCallback)
}