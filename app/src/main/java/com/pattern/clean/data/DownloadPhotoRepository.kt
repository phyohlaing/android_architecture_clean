package com.pattern.clean.data

interface DownloadPhotoRepository {
    fun downloadFromUrl(urlString: String)
    fun setDownloadPhotoCallBack(cb: DownloadPhotoService.DownloadPhotoCallBack): DownloadPhotoRepository
}